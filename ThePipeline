#! /usr/bin/python2
def percentage_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r must be a percentage value in range: [0.0, 1.0]"%(x,))
    return x

def parse_args():
    '''Parse arguments given to script'''

    import argparse

    parser = argparse.ArgumentParser(description="Tuberculosis Genomics Unit " \
        "pipeline for mapping, snp calling, annotation and resistance predic" \
        "tion of MTB sequencing runs")
    subparsers = parser.add_subparsers(help="sub-command help", dest="subcommand")
    # Parser for whole pipeline run (Core Analysis)
    coreparser = subparsers.add_parser("CoreAnalysis", help=("Run the core "
    "analysis pipeline from raw reads to variant calling. Workflow files can "
    "be provided to this command so specific parameters can be changed. "
    "otherwise, the standard pipeline will be run. A sample sheet with "
    "sample-to-fastq correspondence is needed"))
    coreparser.add_argument("-s", "--sample-sheet", dest="ssheet", default="no_ss",
    help=("File with sample to fastq correspondence. Use --sstemplate to create "
    "an example file"))
    coreparser.add_argument("-w", "--workflow", dest="workflow",
    default="no_workflow_provided", help=("File with the workflow to execute "
    "Use --wtemplate to create a workflow template file"))
    coreparser.add_argument("--stemplate", dest="stemplate", action="store_true",
    help=("Create a template sample sheet file"))
    coreparser.add_argument("--wtemplate", dest="wtemplate", action="store_true",
    help=("Create a workflow file template. The template created corresponds to the default workflow"))
    coreparser.add_argument("-p", "--processes", type=int, default=1, dest="proc",
    help=("Most parts of the workflow can be run in parallel for different samples"
    " Specifiy the number of samples that should be analyzed in parallel"
    " Default 1 processor"))

    # PARSER FOR TRIMMOMATIC CLEANING
    trimparser = subparsers.add_parser("trimclean", help=("Clean reads using"
        " trimmomatic with parameters as specified in config file"))
    trimparser.add_argument("-f", "--fastq", dest="fastq",
                                required=True, nargs="*")
    trimparser.add_argument("-c", "--config", dest="cfgfile",
                            default="default_config", help=( "Config file with "
                                                     "trimmomatic parameters"))
    trimparser.add_argument("-t", "--threads", default="1", dest="threads")
    trimparser.add_argument("--log", dest="log", action="store_true", help=(
        "Produce a prefix.trimlog file to trace which reads have been "
        "filtered out and why"))
    trimparser.add_argument("-p", "--prefix", required=True)

    # PARSER FOR FASTP CLEANING
    fastparser = subparsers.add_parser("fastclean", help=("Clean reads using"
        " fastp with parameters as specified in config file"))
    fastparser.add_argument("-f", "--fastq", dest="fastq",
                                required=True, nargs="*")
    fastparser.add_argument("-c", "--config", dest="cfgfile",
                            default="default_config", help=( "Config file with "
                                                     "fastp parameters. Defaults: "
                                                     "--cut_by_quality3; --cut_window_size=10 "
                                                     "--cut_mean_quality=20; --length_required=50 "
                                                     "--correction"))
    fastparser.add_argument("-t", "--threads", default="1", dest="threads")
    fastparser.add_argument("-v", "--verbose", dest="verbose", action="store_true",
        help=("Print the command line used to call fastp in terminal"))
    fastparser.add_argument("--phred64", dest="phred64", action="store_true",
        help=("Use Phred64 scale for input fastq. OUTPUT will be in Phred33"))
    fastparser.add_argument("-p", "--prefix", required=True)


    kparser = subparsers.add_parser("kraken", help=("Classify fastq reads"
        "with kraken and only pick those that match string provided. By default"
        "pick MTBC reads"))
    kparser.add_argument("-f", "--fastq", dest="fastq", required=True, nargs="*")
    kparser.add_argument("-c", "--compressed", dest="compressed",
        action="store_true")
    kparser.add_argument("--paired", dest="paired", action="store_true")
    kparser.add_argument("-m", "--matching", dest="matching",
        default="Mycobacterium tuberculosis")
    kparser.add_argument("-p", "--prefix", dest="prefix", required=True)
    kparser.add_argument("--db", dest="krakenDB", default=False)
    kparser.add_argument("-t", "--threads", dest="threads", default="1")
    kparser.add_argument("--preload", dest="preload", action="store_true")
    kparser.add_argument("--classify", dest="classify", action="store_true")
    kparser.add_argument("--no-labels", dest="nolabels", action="store_true")
    kparser.add_argument("--filter", dest="filterf", action="store_true")
    kparser.add_argument("--noclean", dest="noclean", action="store_true")
    kparser.add_argument("-r", "--report", dest="report", action="store_true",
        help=("Call kraken-report to produce a report from .kraken files"))

    # Parser for kraken-report command
    krparser = subparsers.add_parser("kraken-report", help=("Parse a report"
        " produced with kraken-report and output a ready-to-plot tabular"
        " report file"))
    krparser.add_argument("-p", "--prefix", dest="prefix", required=True)
    krparser.add_argument("-t", "--taxlevel", dest="taxlevel", default="G",
        metavar="Taxlevel in kraken-report format", help=("Formats are for "
            "example G for Genus, or S for species. MTBC and Unclassified"
            " are always in the output"))
    krparser.add_argument("-r", "--report" , dest="report_file", required=True,
        metavar="Kraken-report file to parse")

    # PARSER FOR MAPPING
    mparser = subparsers.add_parser("mapping",
    help=('Map reads against reference with bwa, convert sam output to bam'
          ', sort bam and calculate coverage'))
    mparser.add_argument("-f", "--fastq", dest="fastq", required=True,
        nargs="*")
    mparser.add_argument("-r", "--reference", dest="reference",
        default=False)
    mparser.add_argument("-p", "--prefix", dest="prefix", required=True)
    mparser.add_argument("-t", "--threads", dest="threads", default="1")
    mparser.add_argument("--keepduplicatesbam", dest="keepduplicatesbam", action="store_true")
    mparser.add_argument("-c", "--config", dest="cfgfile",
                            default="default_config", help=("Config file with "
                                                            "picard parameters"))
    mparser.add_argument("-i", "--index", dest="index", action="store_true")
    mparser.add_argument("--no-dedup", dest="nodedup", action="store_true")

    mapfilt = subparsers.add_parser("map_filter", help=("Filter mapping"))
    mapfilt.add_argument("-r", "--reference", dest="reference", default=False)
    mapfilt.add_argument("-p", "--prefix", dest="prefix", required=True)
    mapfilt.add_argument("-e", "--extension", dest="extension", default="bam",
    choices={"bam", "cram"})
    mapfilt.add_argument("--pident", dest="minpident", type=percentage_float,
        metavar="Min Identity; DEF 0.97 from 0 to 1", default=0.97)
    mapfilt.add_argument("--minlen", dest="minlen", type=int,
        metavar="Min Align Len; DEF 40", default=40)
    mapfilt.add_argument("--MAPQ", dest="minMAPQ", type=int,
        metavar="Min MAPQ; DEF 60", default=60)


    # PARSER FOR CALC COVERAGE AND FILTER
    covparser = subparsers.add_parser("coverage",
        help=("Calculate samples coverage from sort.bam files and filter given coverage thresholds"))
    covparser.add_argument("-r", "--reference", dest="reference",
        default=False)
    covparser.add_argument("-e", "--extension", dest="extension", default="bam",
            choices={"bam", "cram"})
    covparser.add_argument("-p", "--prefix", dest="prefix", required=True)
    # covparser.add_argument("--runID", dest="runID", default=None)
    covparser.add_argument("-f", "--filter", dest="filter", action="store_true",
        help=("Activate the coverage filter. Samples not passing thresholds are"
            " moved to NoPassCov/"))
    covparser.add_argument("--min-mean", dest="minmean", default=0, metavar="DEF: 0",
        type=int)
    covparser.add_argument("--min-median", dest="minmedian", default=20, metavar="DEF: 20",
        type=int)
    covparser.add_argument("--min-coverage", dest="mincov", default=0.95, metavar="DEF: 0.95",
        type=percentage_float)
    covparser.add_argument("--keep-coverage", dest="keepcoverage", action="store_true",
        help=("Keep .coverage files"))
    covparser.add_argument("--depth4cov", dest="depth4cov", default=10,
     help=("Minimum depth to consider covered a base, DEF: 10"), type=int)

    # PARSER FOR SNP and INDEL CALLING
    callparser = subparsers.add_parser("calling", help="calling help")
    callparser.add_argument("--pipeline", dest="pipeline", default="standard",
    	choices={"direct-sample", "long-amplicon", "long-genomic"})

    callparser.add_argument("-e", "--extension", dest="ext", default=".sort.bam",
        help=("Extension of mapped files to perform variant calling on. DEF: .sort.bam"))
    callparser.add_argument("-r", "--reference", dest="reference",
        default=False)
    callparser.add_argument("-p", "--prefix", dest="prefix", required=True)
    # callparser.add_argument("--filter-by-density", dest="density",
    #    action="store_true", help=("Filter SNPs according to its density and"
    #        " proximity to indels"))
    callparser.add_argument("--noclean", dest="noclean", action="store_true")
    callparser.add_argument("-i", "--indel-freq", dest="indelfreq", default=0.5,
        type=percentage_float, metavar="min indel frequency", help=("Filter indels "
            "with frequency below specified. DEFAULT=0.5"))
    callparser.add_argument("--window-density", dest="wdensity", default="10",
        metavar="bp window", help=("bp window for density filter. DEFAULT=10"))
    callparser.add_argument("--window-indel", dest="windel", default="0",
        metavar="bp window", help=("bp window for indel filter. DEFAULT=0"))
    # Parser for annotation filter
    annoF_parser = subparsers.add_parser("annotation_filter", help="filter PPE, phage, mobile and"
        " repetitive regions from *.snp.final files")
    annoF_parser.add_argument("-s", "--snp-file", dest="snp_file", required=True)
    annoF_parser.add_argument("--no-header", dest="noheader", action="store_true")
    annoF_parser.add_argument("-f", "--field", dest="field", default="2",
                             metavar="Field of positions in file. DEFAULT=2")
    annoF_parser.add_argument("--sep", dest="sep", metavar="field delimiter. DEFAULT=TAB",
        default="\t")

    # PARSER FOR DELETIONS FILTERING
    delF_parser = subparsers.add_parser("del_filter", help="filter SNPs that have been "
        "called near deletions")
    delF_parser.add_argument("-s", "--snp-file", dest="snp_file", required=True)
    delF_parser.add_argument("-d", "--del-file", dest="del_file", required=True)
    delF_parser.add_argument("-f", "--field", dest="field", default=2, type=int,
                             metavar="Field of positions in file. DEFAULT=2")
    delF_parser.add_argument("--del-sep", dest="del_sep", metavar="field delimiter for .del file. DEFAULT=Space",
        default=" ")
    delF_parser.add_argument("--snp-sep", dest="snp_sep", metavar="field delimiter for .snp file. DEFAULT=TAB",
        default="\t")
    delF_parser.add_argument("--distance", dest="distance", default=2, type=int,
        metavar="Distance of a SNP from a deleted position to be filtered out. DEFAULT=2")

    # PARSER FOR DENSITY FILTERING
    densF_parser = subparsers.add_parser("density_filter", help="filter SNPs by density")
    densF_parser.add_argument("-s", "--snp-file", dest="snp_file", required=True)
    densF_parser.add_argument("-f", "--field", dest="field", default=2, type=int,
                             metavar="Field of positions in file. DEFAULT=2")
    densF_parser.add_argument("--snp-sep", dest="snp_sep", metavar="field delimiter for .snp file. DEFAULT=TAB",
        default="\t")
    densF_parser.add_argument("--window", "-w", dest="window", default=10, type=int,
        metavar="Max distance between SNPs to be considered as candidate false positives. DEFAULT=10")
    densF_parser.add_argument("--max-density", dest="max_density", default=3, type=int,
        metavar="Max distance between SNPs to be considered as candidate false positives. DEFAULT=3")

    mergeparser = subparsers.add_parser("mergesnp", help="Combine snp files to create a single table")
    mergeparser.add_argument("-s", "--snp", nargs="*", metavar="SNP_FILE(S) to merge",
        dest="snp_files", required=True, help="EXAMPLE: -s *.DR.snp.final")
    #mergeparser.add_argument("-d", "--del", dest="deletions", action="store_true",
    #    help=("Wether the mergesnp table should include deletions or not. In that "
    #         "case, the .del files must be in the same folder than the snps being merged"))

    rescueparser = subparsers.add_parser("rescue", help=("Given a snp file and a"
        " mergesnp table, rescue lost SNPs from original .snp file according to"
        " different parameters"))
    rescueparser.add_argument("-m", "--mergesnp", dest="mergesnp",
        metavar="mergesnp table (like SNP table)", required=True)
    rescueparser.add_argument("--rescue-from", dest="rescue_from", required=True,
        help=("SNP file variants are going to be rescued from if they pass"
            " the filters"), metavar="SAMPLE.snp")
    rescueparser.add_argument("--rescue-to", dest="rescue_to", required=True,
        help=("SNP file variants are going to be recovered and added to if"
            " they pass the filters"), metavar="SAMPLE.EPI.final")
    # rescueparser.add_argument("-d", "--del", dest="delfh", required=True,
  #      help=(".snp.del file"), metavar="SAMPLE.snp.del")
    rescueparser.add_argument("-p", "--prefix", dest="prefix", required=True,
        help=("Prefix of that sample to read from $PREFIX.snp file and to search"
            " for that sample in mergesnp table. This implies that, if we're"
            " for example working with SampleX.snp. The column of alleles for"
            " SampleX has to have the header <<SampleX>>"))
    rescueparser.add_argument("--min-num-strains", dest="min_num", default=1,
        help=("Minimum number of strains in mergesnp table holding a SNP in order"
            " to rescue it from the snp file"), type=float)
    rescueparser.add_argument("--min-perc-strains", dest="min_perc", default=0,
        help=("Minimum percentage of strains in mergesnp table holding a SNP in order"
            " to rescue it from the snp file"), type=percentage_float)
    rescueparser.add_argument("--min-reads2", dest="min_reads2", default=0,
        help=("When a SNP is to be rescued, minimum number of reads supporting that SNP"
            " in order to actually rescue the SNP"), type=int)
    rescueparser.add_argument("--both-strands", dest="both_strands", action="store_true",
        help=("When a SNP is to be rescued, whether it has to be read in both, forward"
            " and reverse direction"))
    rescueparser.add_argument("--min-cov", dest="min_cov", default=1,
        help=("When a SNP is to be rescued, min coverage in that position calculated"
            " as sum of reads1 and reads2 in order to actually rescue the SNP"), type=int)
    rescueparser.add_argument("--min-freq", dest="min_freq", default=0.5,
        help=("When a SNP is to be rescued, min freq of the SNP to be actually rescued"), 
        type=percentage_float)

    rparser = subparsers.add_parser("resistance", help="resistance prediction help")
    rparser.add_argument("-s", "--snp", metavar="SNP_FILE to calc resistance from",
        dest="snp_file", required=True)
    rparser.add_argument("-i", "--indel", metavar="INDEL_FILE to calc resistance from",
    dest="indel_file", required=True)
    rparser.add_argument("-d", "--del", metavar="SNP_FILE to calc resistance from",
        dest="del_file", required=True)
    rparser.add_argument("-p", "--prefix", metavar="SAMPLE_NAME",
    help=("DR predicition will be written to SAMPLE_NAME.DRprediction. If no"
    " prefix is provided then prediction is written to stdout"), default="no_prefix",
    dest="prefix")
    rparser.add_argument("--output-unknown", dest="output_unknown", action="store_true",
        help=("Output snps in positions not described as DR but in DR-associated genes"))

    distparser = subparsers.add_parser("distance", help="calculate distances for SNP files provided")
    distparser.add_argument("-s", "--snp", nargs="*", metavar="SNP_FILE(S) to calc distance from",
        dest="snp_files", required=True, help="EXAMPLE: -s *.EPI.annofilter.rescued.snp.final")
    report = subparsers.add_parser("report", help=("Make a summary report of two"
         " pipeline analysis of the same dataset"))
    report.add_argument("-c", "--clean", dest="clean", required=True,
     metavar="PATH TO CLEAN DATASET")
    report.add_argument("-d", "--dirty", dest="dirty", required=True,
     metavar="PATH TO DIRTY DATASET")
    report.add_argument("-r", "--resistance", dest="resistance",
     action="store_true")
    report.add_argument("-s", "--suffix", dest="suffix", required=True)
    # # Parser for version control
    # vparser = subparsers.add_parser("version",
    #     help=('Print programs, data and PipeModules version'))
    contaminants = subparsers.add_parser("contaminants",
        help=("Write genus and species contaminants for each sample from kraken.reports"))
    contaminants.add_argument("-p", "--prefix", required=True, dest="prefix")

    args = parser.parse_args()

    return args


def RunSubcommand(args):
    ''' Run subcommand with proper arguments'''
    import sys

    # Execute subcommand
    if args.subcommand == "CoreAnalysis":
        from PipeModules.CoreAnalysis import CoreAnalysis
        CoreAnalysis(args)

    elif args.subcommand == "trimclean":
        from PipeModules.TrimClean import TrimClean
        TrimClean(args)

    elif args.subcommand == "fastclean":
        from PipeModules.FastClean import FastClean
        FastClean(args)

    elif args.subcommand == "pipeit":
        from PipeModules.Pipe import Pipeit
        Pipeit(args)

    elif args.subcommand == "kraken":
        from PipeModules.Kraken import Kraken
        Kraken(args)

    elif args.subcommand == "kraken-report":
        from PipeModules.Kraken import KrakenReport
        KrakenReport(args)

    elif args.subcommand == "mapping":
        from PipeModules.Mapping import Mapping
        Mapping(args)

    elif args.subcommand == "map_filter":
        from PipeModules.Map_Filter import FilterMapping
        sys.stderr.write("\033[94mFiltering mapping from sort.bam files\n\033[0m")
        FilterMapping(args)

    elif args.subcommand == "coverage":
        from PipeModules.Coverage import CalcCoverage
        CalcCoverage(args)

    elif args.subcommand == "calling":
    	if args.pipeline == "standard":
        	from PipeModules.Calling import Calling
        elif args.pipeline == "long-amplicon":
        	from PipeModules.CallingLA import Calling
        elif args.pipeline == "long-genomic":
            from PipeModules.CallingLG import Calling
        elif args.pipeline == "direct-sample":
        	from PipeModules.CallingDS import Calling
        else:
        	assert False
        Calling(args)

    elif args.subcommand == "annotation_filter":
        from PipeModules.Anno_Filter import FilterSnps
        sys.stderr.write("\033[94mFiltering PPE/repeat/phage regions\n\033[0m")
        FilterSnps(args)

    elif args.subcommand == "del_filter":
        from PipeModules.Del_Filter import FilterByDel
        sys.stderr.write("\033[94mFiltering near-deletion SNPs\n\033[0m")
        FilterByDel(args)

    elif args.subcommand == "density_filter":
        from PipeModules.Density_Filter import FilterByDensity
        sys.stderr.write("\033[94mFiltering SNPs by density\n\033[0m")
        FilterByDensity(args)

    elif args.subcommand == "mergesnp":
        from PipeModules.Mergesnp import WriteMergeDict
        sys.stderr.write("\033[94mBuilding mergesnp table\n\033[0m")
        WriteMergeDict(args)

    elif args.subcommand == "rescue":
        from PipeModules.RescueSNP import Rescue_SNP_File
        sys.stderr.write("\033[94mRescuing SNPs from final dataset\n\033[0m")
        Rescue_SNP_File(args)

    elif args.subcommand == "resistance":
        from PipeModules.Resistance import Resistance
        Resistance(args)

    elif args.subcommand == "report":
        from PipeModules.Reports import CompareRun
        CompareRun(args)

    elif args.subcommand == "contaminants":
        from PipeModules.Contaminants import AssessContam
        AssessContam(args.prefix)

    elif args.subcommand == "distance":
        from PipeModules.Distance import CalcDistance
        CalcDistance(args)

    elif args.subcommand == "report":
        from PipeModules.Reports import CompareRun
        CompareRun(args)

def main():

    # Parse args
    args = parse_args()
    RunSubcommand(args)




if __name__ == "__main__":
    main()
