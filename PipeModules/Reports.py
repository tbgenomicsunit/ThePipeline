
def ParseSNP(snp_file, known_res, unknown_res):
    '''Parse a .snp file and return a dict with summary stats for some columns

    also check if a snp falls in a known-resistance-associated position
    or an unknown-resistance-associated position both sets provided

    '''
    import numpy as np

    # Dict to store stats
    stats = {}
    # Counter to count snps, htero and homozygote positions
    nsnps = 0.0
    hetero = 0.0
    homo = 0.0
    # Counters to count known and unknown resistance mutations
    # and known and unknown heterozygote resistance mutation
    known = 0.0
    unknown = 0.0
    known_hetero = 0.0
    unknown_hetero = 0.0
    # Set to store heterozygote positions and known and unknown heterozygote
    # resistance mutations
    pos_hetero = set()
    pos_homo = set()
    pos_known_hetero = set()
    pos_unknown_hetero = set()
    # List to store freqs
    freqs = []

    with open(snp_file) as infile:
        # Read header
        line = infile.readline()
        for line in infile:
            line = line.split()
            pos = int(line[1])
            nsnps += 1
            freq = line[6]
            heterozygote = False
            # To store freq as a float, first remove % symbol
            freq = freq.rstrip("%")
            # replace commas by dots to float nums O_o
            freq = freq.replace(",", ".")
            freq = float(freq)
            if freq < 90:
                heterozygote = True
            if heterozygote:    
                hetero += 1.0
                pos_hetero.add(pos)
                if pos in known_res:
                    known += 1.0
                    known_hetero += 1.0
                    pos_known_hetero.add(pos)
                elif pos in unknown_res:
                    unknown += 1.0
                    unknown_hetero += 1.0
                    pos_unknown_hetero.add(pos)
                freqs.append(freq)

            else:
                homo += 1.0
                if pos in known_res:
                    known += 1
                elif pos in unknown_res:
                    unknown += 1
                pos_homo.add(pos)

    if nsnps == 0:
        stats["nsnps"] = 0
        stats["hetero"] = 0
        stats["homo"] = 0
        stats["perc_hetero"] = 0
        stats["perc_homo"] = 0
        stats["freqs"] = "{}-{}-{}-{}-{}".format(0,0,0,0,0)
        stats["known_res"] = 0
        stats["unknown_res"] = 0
        stats["pos_hetero"] = pos_hetero
        stats["pos_known_hetero"] = pos_known_hetero
        stats["pos_unknown_hetero"] = pos_unknown_hetero
        stats["pos_homo"] = pos_homo
        return stats

    else:
        if not freqs:
            stats["freqs"] = "{}-{}-{}-{}-{}".format(0,0,0,0,0)
        else:
            freq_min = min(freqs)
            freq_25 = np.percentile(freqs, 25)
            freq_50 = np.percentile(freqs, 50)
            freq_75 = np.percentile(freqs, 75)
            freq_max = max(freqs)
            stats["freqs"] = "{}-{}-{}-{}-{}".format(freq_min, freq_25,
                                                     freq_50, freq_75, freq_max)
        perc_hetero = hetero / nsnps
        perc_homo = homo / nsnps
        stats["nsnps"] = int(nsnps)
        stats["hetero"] = int(hetero)
        stats["homo"] = int(homo)
        stats["perc_hetero"] = perc_hetero
        stats["perc_homo"] = perc_homo
        stats["known_res"] = int(known)
        stats["unknown_res"] = int(unknown)
        stats["pos_hetero"] = pos_hetero
        stats["pos_known_hetero"] = pos_known_hetero
        stats["pos_unknown_hetero"]= pos_unknown_hetero
        stats["pos_homo"] = pos_homo

        return stats

def MergeSNP(snp1, snp2):
    '''Compare two snp stats (thought to be snp2 and snp1) dicts parsed by 
    ParseSNP and prepare another dict ready to by written'''

    # Calculate how many positions that were heterozygotes in snp1 sample
    # are known homozygotes
    hetero_pos_dirty = snp1["pos_hetero"]
    homo_pos_clean = snp2["pos_homo"]
    now_homocygote = hetero_pos_dirty.intersection(homo_pos_clean)
    #remain_hetero = hetero_pos_snp1.intersection(hetero_pos_snp2)

    hetero_known_snp1 = snp1["pos_known_hetero"]
    hetero_known_snp2 = snp2["pos_known_hetero"]
    known_now_homo = hetero_known_snp1.difference(hetero_known_snp2)

    hetero_unknown_snp1 = snp1["pos_unknown_hetero"]
    hetero_unknown_snp2 = snp2["pos_unknown_hetero"]
    unknown_now_homo = hetero_known_snp1.difference(hetero_known_snp2)
    
    mergesnp = {}
    mergesnp["reads"] = "{}:{}".format(int(snp1["reads"]), 
                                              int(snp2["reads"]))
    mergesnp["MTBC"] = "{:.2f}".format(100*snp2["reads"] / snp1["reads"])
    mergesnp["coverage"] = "{:.2f}:{:.2f}".format(snp1["coverage"], 
                                                  snp2["coverage"])
    mergesnp["nsnps"] = "{}:{}".format(snp1["nsnps"], snp2["nsnps"])
    mergesnp["homo"] = "{}:{}".format(snp1["homo"], snp2["homo"])
    mergesnp["hetero"] = "{}:{}".format(snp1["hetero"], snp2["hetero"])
    mergesnp["perc_homo"] = "{:.2f}:{:.2f}".format(snp1["perc_homo"]*100, 
                                                   snp2["perc_homo"]*100)
    mergesnp["perc_hetero"] = "{:.2f}:{:.2f}".format(snp1["perc_hetero"]*100, 
                                                     snp2["perc_hetero"]*100)
    mergesnp["freqs"] = "{}:{}".format(snp1["freqs"], snp2["freqs"])
    mergesnp["known_res"] = "{}:{}".format(snp1["known_res"], 
                                      snp2["known_res"])
    mergesnp["unknown_res"] = "{}:{}".format(snp1["unknown_res"], 
                                        snp2["unknown_res"])
    mergesnp["hetero2homo"] = "{}".format(len(now_homocygote))
    mergesnp["known2homo"] = "{}".format(len(known_now_homo))
    mergesnp["unknown2homo"] = "{}".format(len(unknown_now_homo))
    mergesnp["become_homo"] = "{}".format(now_homocygote)
    #mergesnp["remain_hetero"] = "{}".format(remain_hetero)

    return mergesnp

def GetCoverage(meancov):
    '''Get coverage value from .meancov file'''
    with open(meancov) as infile:
        sample, coverage = infile.readline().rstrip().split()

    return float(coverage)

def CountReads(path_to_sample):
    '''Count number of original reads from sample.kraken file and number
    of used reads from sample.filtered.readlist and calc the percentage of
    reads finally used'''

    with open("{}.nfilter".format(path_to_sample)) as infile:
        # Count total reads
        line = infile.readline()
        line = line.rstrip().split(":")
        reads = int(line[1])
        # Count MTBC reads
        line = infile.readline()
        line = line.rstrip().split(":")
        MTBC_reads = int(line[1])

    return (reads, MTBC_reads)

def WriteSNP(samples):
    ''' Write to standard output snp info of each sample'''
    import sys

    header = (
    "Sample\t"
    "reads\t"
    "%MTBC\t"
    "coverage\t"
    "n_snps\t"
    "homo\t"
    "hetero\t"
    "%homo\t"
    "%hetero\t"
    "freqs-hetero\t"
    "known_res_snp\t"
    "unknown_res_snp\t"
    "hetero2homo\t"
    "known_hetero_res_2homo\t"
    "unknown_hetero_res_2homo\n")
    sys.stdout.write(header)
    for sample in samples:
        stats = samples[sample]
        line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
            sample,
            stats["reads"],
            stats["MTBC"],
            stats["coverage"],
            stats["nsnps"],
            stats["homo"],
            stats["hetero"],
            stats["perc_homo"],
            stats["perc_hetero"],
            stats["freqs"],
            stats["known_res"],
            stats["unknown_res"],
            stats["hetero2homo"],
            stats["known2homo"],
            stats["unknown2homo"],
            #stats["become_homo"],
            #stats["remain_hetero"]
            )
        sys.stdout.write(line)

    return 0
    
def ParseDrugs(drug_file):
    '''Return a dictionary with the information from resistnace prediction file'''
    prediction = {}
    with open(drug_file) as infile:
        header = infile.readline().rstrip()
        drugs = header.split("\t")
        for line in infile:
            line = line.rstrip().split("\t")
            sample = line[0]
            prediction[sample] = {}
            for i in range(len(line[1:])):
                drug = drugs[i]
                prediction[sample][drug] = line[i]
    return prediction

def ComparePrediction(clean_prediction, dirty_prediction):
    '''Compare prediction dictionaries parsed with ParseDrugs
    The comparison is stored in dict in the following manner:

    { sample1 : ["INH@+", "RMP@-"] } This would mean that for sample1 there is no
    difference for other drugs than isoniazid and rifampin, rifampin became 
    resistan and rifampin susceptible
    '''
    comparison = {}

    for sample in dirty_prediction:
        comparison[sample] = {}
        dirty = dirty_prediction[sample]
        clean = clean_prediction[sample]
        for drug in dirty:
            dirty_drug_pred = dirty[drug]
            clean_drug_pred = clean[drug]
            if dirty_drug_pred == "Resistant" and \
                 clean_drug_pred == "Resistant":
                 comparison[sample][drug] = "{}+@+".format(drug)
            elif dirty_drug_pred == "Susceptible" and \
                 clean_drug_pred == "Susceptible":
                comparison[sample][drug] = "{}-@-".format(drug)
            elif dirty_drug_pred == "Resistant" and \
                 clean_drug_pred == "Susceptible":
                 comparison[sample][drug] = "{}+@-".format(drug)
            elif dirty_drug_pred == "Susceptible" and \
                 clean_drug_pred == "Resistant":
                comparison[sample][drug] = "{}-@+".format(drug)
            else:
                assert False

    return comparison

def WriteDrugs(comparison, clean):
    '''Write to comparison result to RESISTANCE.comparison in clean directory'''

    with open("{}/RESISTANCE.comparison".format(clean), "w") as outfile:
        for sample in comparison:
            if comparison[sample]:
                line = sample
                for drug in comparison[sample]:
                    line += "\t{}".format(comparison[sample][drug])
                line += "\n"
                outfile.write(line)

def CompareRun(args):
    '''This function takes a path where a run of ThePipeline has been completed
    and files .snp.final have been created and generates a table of summary
    statistics for all samples. Two paths can be passed when comparing runs.
    Then summary statistics are merged for both runs and a single summary table
    created'''
    import os
    from ListFiles import ListSingle
    from Resistance import IntersectPositions

    dirty = os.path.realpath(args.dirty)
    clean = os.path.realpath(args.clean)

    known_res, unknown_res = IntersectPositions()

    # Get a list of .snp.files
    snp_files = ListSingle(dirty, ".{}".format(args.suffix))
    samples = {}
    for snp in snp_files:
        # Get the name of the sample by trimming .snp.final
        sample = snp.split("/")[-1] # Get last element from snp_file path
        sample = sample.replace(".{}".format(args.suffix), "")
        # Get coverage for each run
        coverage1 = GetCoverage("{}/{}.meancov".format(dirty, sample))
        coverage2 = GetCoverage("{}/{}.meancov".format(clean, sample))
        reads, MTBC_reads = CountReads("{}/{}".format(clean, sample))
        # Get snp stats for each run
        dirty_snp = "{}/{}.{}".format(dirty, sample, args.suffix)
        clean_snp = "{}/{}.{}".format(clean, sample, args.suffix)
        dirty_stats = ParseSNP(dirty_snp, known_res, unknown_res)
        clean_stats = ParseSNP(clean_snp, known_res, unknown_res)
        dirty_stats["coverage"] = coverage1
        dirty_stats["reads"] = reads
        clean_stats["coverage"] = coverage2
        clean_stats["reads"] = MTBC_reads
        mergestats = MergeSNP(dirty_stats, clean_stats)
        samples[sample] = mergestats
    # If resistance provided, compare also drug susceptibility
    # predictions
    if args.resistance:
        clean_res = "{}/RESISTANCE.snp.final.result".format(clean)      
        dirty_res = "{}/RESISTANCE.snp.final.result".format(dirty)
        clean_prediction = ParseDrugs(clean_res)
        dirty_prediction = ParseDrugs(dirty_res)
        comparison = ComparePrediction(clean_prediction, dirty_prediction)
        WriteDrugs(comparison, clean)

            

    # Write report to stdout
    WriteSNP(samples)

    return 0