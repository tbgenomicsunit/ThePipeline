def CalcDistance(args):
    '''Just call Alvaro scripts to build the multifasta and
    calc the distances between samples'''
    from subprocess import call
    import sys

    TempFiles(args.snp_files)

    stat = call(["bash", "/data/ThePipeline/PipeModules/PipeScripts/bashSNP_impact_prediction_yersin"])
    if stat != 0:
        sys.exit("Pipeline stopped when building the multifasta\n")

    stat = call(["Rscript", "/data/ThePipeline/PipeModules/PipeScripts/calculate_distances_yersin.R"])
    if stat != 0:
        sys.exit("Pipeline stopped when calculating distances\n")

    RmTempFiles(args.snp_files)

    return 0

def TempFiles(snp_files):
    ''' Los script de calcular resistencias de alvaro no permiten dar argumentos 
    si no los pones a mano en el Script. Por ejemplo si quiero que la 
    resistencia la calcule sobre los .snp, tendria que ir al script original y 
    modificar eso. Lo que se me ha ocurrido para bypasear esto, es que
    ThePipeline acepte los snp sobre los que se quieran calcular las resistencias
    y cree una copia de estos archivos cuya extension sera .resistance.tmp

    El script de alvaro siempre aceptara como archivos de snp los acabados en esa
    extension

    Cuando se calcule las resistencias, entonces se eliminan los archivos con
    esa extension
    '''
    import shutil

    for snp in snp_files:
        shutil.copy(snp, "{}.distance.tmp".format(snp))

    return 0

def RmTempFiles(snp_files):
    '''Eliminar los archivos temporales creados con TempFiles'''
    import os
    for snp in snp_files:
        os.remove("{}.distance.tmp".format(snp))

    return 0