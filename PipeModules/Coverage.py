def CoverageBAM(prefix, reference):
    ''' Calculate coverage '''
    from subprocess import call
    from Repository import Programs, Data
    import sys

    # genomeCoverageBed is a script that is under bedtools/bin folder
    programs = Programs()
    genomeCoverageBed = programs["genomeCoverageBed"]

    bamfile = "{}.sort.bam".format(prefix)
    covfile = "{}.coverage".format(prefix)
    with open(covfile, "w") as outfh:
        stat = call([genomeCoverageBed, "-ibam", bamfile, "-d", "-g", reference],
                     stdout=outfh)
    if stat != 0:
        sys.exit("Pipeline stopped when calculating each bam coverage!\n")

    return 0

def CoverageCRAM(prefix, reference):
    ''' Calculate coverage '''
    import subprocess as sp
    from Repository import Programs, Data
    import sys


    # genomeCoverageBed is a script that is under bedtools/bin folder
    programs = Programs()
    genomeCoverageBed = programs["genomeCoverageBed"]
    samtools = programs["samtools"]

    cramfile = "{}.sort.cram".format(prefix)
    covfile = "{}.coverage".format(prefix)
    with open(covfile, "w") as outfh:
        view = sp.Popen([samtools, "view", "-b", cramfile], stdout=sp.PIPE)

        genomeCov = sp.Popen([genomeCoverageBed, "-ibam", "stdin", "-d",
                              "-g", reference], stdin=view.stdout, stdout=outfh)
        view.stdout.close()
        out, err = genomeCov.communicate()


    return 0

def MeanCoverage(prefix, depth4cov):
    ''' Calculate the mean coverage per pb in a sample from
    prefix.coverage file '''

    positions = 0.0
    coverage = 0
    covered_positions = 0.0
    # Add cov list to sort and calc median
    covlist = []
    with open("{}.coverage".format(prefix)) as infile:
        for line in infile:
            if line == "":
                positions = 0.0
                break
            ref, pos, cov = line.rstrip().split()
            cov  = int(cov)
            positions += 1.0
            coverage += cov
            if cov >= depth4cov:
                covered_positions += 1.0
            covlist.append(int(cov))

    if positions > 0:
        # Calc mean
        mean_cov = coverage / positions
        # Calc median
        # Recall that position X in a list is accesed with X-1
        # so for example the 6th element is accessed as covlist[5]
        covlist.sort()
        if len(covlist) % 2 == 0:
            m1 = len(covlist) / 2
            cov1 = covlist[m1 - 1]
            cov2 = covlist[m1]
            median_cov = (cov1 + cov2) / 2.0
        else:
            m1 = len(covlist) / 2
            # not neccessary tu sum 1 as the m1 +1 element is
            # accessed in fact with just m1
            median_cov = covlist[m1]

        genome_coverage = covered_positions / positions
        return (mean_cov, median_cov, genome_coverage)
    else:
        return (0,0,0)

def MeanSampleCoverage(prefix, keepcoverage, depth4cov):
    import os

    meancov, mediancov, genome_coverage = MeanCoverage(prefix, depth4cov)
    with open("{}.meancov".format(prefix), "w") as outfh:
        outfh.write("{}_mean_depth\t{}\n".format(prefix, meancov))
        outfh.write("{}_median_depth\t{}\n".format(prefix, mediancov))
        outfh.write("{}_genome_coverage\t{}\n".format(prefix, genome_coverage))

    # Remove .coverage files
    if not keepcoverage:
        os.remove("{}.coverage".format(prefix))

    return meancov, mediancov, genome_coverage

def FiltByCov(prefix, mean, median, cov, minmean, minmedian, mincov):
    '''If a sample do not pass coverage/depth thresholds, move all its files
    to NoPassCov folder'''
    import os, shutil
    from glob import glob

    if mean < minmean or median < minmedian or cov < mincov:
        # Create NoPassCov if it does not exist
        try:
            os.mkdir("NoPassCov")
        except OSError:
            pass

        files = glob("{}*".format(prefix))
        for file in files:
            shutil.move(file, "./NoPassCov/{}".format(file))

    return 0

# def SummaryRunCoverage(run_ID=None):
#     '''Create a tab file with samples in first column and coverage
#     in second column'''
#     from subprocess import call
#     import sys

#     with open("Run{}.summary.coverage", "w") as outfh:
#         stat = call(["grep", ".", "*.meancov"], stdout=outfh)
#     if stat != 0:
#         sys.exit("Pipeline stopped at summaryzing mean coverages\n")
#     # And remove *.meancov files
#     stat = call(["rm", "*.meancov"])
#     if stat != 0:
#         sys.exit("Pipeline stopped when removing *.meancov files\n")

#     return 0

def CalcCoverage(args):
    from Repository import Programs, Data

    data = Data()

    prefix = args.prefix
    reference = args.reference
    keepcoverage = args.keepcoverage
    depth4cov = args.depth4cov

    if not reference:
        reference = data["reference"]

    if args.extension == "bam":
        # Calc coverage given a sort.bam
        CoverageBAM(prefix, reference)
        # Create the .meancov file and get its values
    elif args.extension == "cram":
        CoverageCRAM(prefix, reference)
    else:
        assert False

    mean, median, cov = MeanSampleCoverage(prefix, keepcoverage, depth4cov)

    # Filter by Coverage in case it is specified
    if args.filter:
        FiltByCov(args.prefix, mean, median, cov,
                  args.minmean, args.minmedian, args.mincov)

    return 0
