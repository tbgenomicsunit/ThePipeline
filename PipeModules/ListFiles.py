def ListPaired(folder, suffix):
    '''List paired files that meet some suffix criteria and
    return them in a list of tuples
     
    This function is intended to work with paired files that hold
    R1 and R2 chars to denote forward and reverse read

    e.g [(s1_R1.fastq, s1_R2.fastq), (s2_R1.fastq, s2_R2.fastq)]
    '''
    import os
    import re
    import glob

    
    # Get full path of working directory
    wdir = os.path.realpath(folder)
    pattern = "{}/*{}".format(wdir, suffix)
    files = glob.glob(pattern)

    # Get a set of uniq preffixes that identify samples
    samples = set(re.split("R[12]", fastq)[0] for fastq in files)
    # Build list of paired samples (in tuples)
    paired = [("{}R1{}".format(sample, suffix), 
               "{}R2{}".format(sample, suffix)) for sample in samples]

    return paired

def ListSingle(folder, suffix):
    '''List single files that meet some suffix cirteria and return them
    in a list. 

    e.g for ".fastq" --> [sample1.fastq, sample2.fastq, sample3.fastq]
    '''
    import os
    import glob
    import re

    # Get full path of working directory
    wdir = os.path.realpath(folder)
    pattern = "{}/*{}".format(wdir, suffix)
    files = glob.glob(pattern)

    return files
