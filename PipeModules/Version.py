# Este modulo es para obtener la version de los programas. He intentado hacerlo
# de un modo fancy, cogiendo la version del output de cada programa, pero cada
# uno es de su padre y su madre asa que habra que mantener la lista actualizada
def version(program):
	'''Read the program versions from PipeModules/PipeConfigs/sofware_versions.txt'''
	import os

	path = os.path.dirname(os.path.abspath(__file__))

	versions = {}
	with open("{}/PipeConfigs/software_versions.txt".format(path)) as infile:
		for line in infile:
			soft, version = line.rstrip().split("=")
			versions[soft] = version

	return versions[program]


def ThePipelineV():
	'''This is a suuperspecial module that stores major Pipeline versions. It
	must return both the name of the version and a list of major changes.
	The changes of consecutive versions will be referred to the original version
	'''

	TP_version = "Tris1M"
