#! /bin/bash

# This is script is called by ThePipeline
# with PREFIX. So $1 will be the prefix
# used like Sample1

PREFIX=$1
RSCRIPT=$2
# Regions with 0 coverage in mpileup
awk '{print $2, $4}' $PREFIX.mpileup.remove | 
  awk '$1!=p+1{print p+1"\t"$1-1}{p=$1}' > $PREFIX.pileup_indel.remove

# SNPs with less than 3 coverage are considered N
awk '{print $2, $4}' $PREFIX.mpileup.remove | 
  sort -k2 -n | 
    awk '$2<3{print $1}' > $PREFIX.pileup_min_cov.remove 

# Remove SNPs in high density regions (3 or more SNPs in 10bp) and SNPs close to indel (10 bp) using the .indel and the .indel.remove files
# Rscript $RSCRIPT $PREFIX.DR.snp.final $PREFIX.snp.indel $PREFIX.pileup_indel.remove -window_density 10 -window_indel 10 -output $PREFIX.DR.snp.final.densityfilter


# Generate a deletion file combining those with 0 coverage in pileup, those <3 and those from indel file alignment
awk '{print $1, $2, $3, $3}' $PREFIX.snp.indel | 
  sed 's/ [A-Z]/ N/g' > $PREFIX.snp.del.remove # deletions from Varscan indel alignment

awk '{print "MTB_anc", $1, "N","N"}' $PREFIX.pileup_min_cov.remove >> $PREFIX.snp.del.remove # deletions with cov < 3 
cat $PREFIX.pileup_indel.remove | while IFS='' read -r line || [[ -n "$line" ]]; do
seq $line >> $PREFIX.pileup_indel_positions.remove
done
awk '{print "MTB_anc", $1, "N","N"}' $PREFIX.pileup_indel_positions.remove >> $PREFIX.snp.del.remove # positions not present in mpileup
sort -u -k2,2n $PREFIX.snp.del.remove > $PREFIX.snp.del # sort and deduplicate