def parse_sample_sheet(sample_sheet):
    '''Parse a sample sheet that must be a tabular file with
    PREFIX \t FASTQ1 \t FASTQ2
    '''
    with open(sample_sheet) as infile:
        for line in infile:
            yield line.rstrip().split(";")

def parse_workflow(workflow):
    '''Parse ThePipeline workflow file to build a CMD to run all the workflow
    commands'''
    workflow_CMD = [] # List to store all the commands to run
    newcmd = None
    with open(workflow) as infile:
        for line in infile:
            if line and not line.startswith("#"):
                line = line.rstrip()
                if line.startswith("!"):
                    if newcmd != None:
                        workflow_CMD.append(newcmd)
                    newcmd =  [ line.lstrip("!") ]
                elif line.startswith("--"):
                    newcmd.extend(line.split())
    workflow_CMD.append(newcmd)
    return workflow_CMD

def call_command( args ):
    '''Given a Pipeline command and parameters build the specific
    CMDs and call them as subprocesses'''
    import sys
    import subprocess as sp

    command, params, prefix, R1, R2 = args

    if command == "fastclean":
        CMD = [ "ThePipeline",
                "fastclean",
                "-f", R1, R2,
                "-p", prefix]
        CMD.extend(params)
    elif command == "trimclean":
        CMD = [ "ThePipeline",
                "trimclean",
                "-f", R1, R2,
                "-p", prefix]
        CMD.extend(params)
    elif command == "kraken":
        CMD = [ "ThePipeline",
                "kraken",
                "-f", R1, R2,
                "-p", prefix]
        CMD.extend(params)
    elif command == "mapping":
        CMD = [ "ThePipeline",
                "mapping",
                "-f", R1, R2,
                "-p", prefix]
        CMD.extend(params)
    elif command == "coverage":
        CMD = [ "ThePipeline",
                "coverage",
                "-p", prefix]
        CMD.extend(params)
    elif command == "calling":
        CMD = [ "ThePipeline",
                "calling",
                "-p", prefix]
        CMD.extend(params)
    else:
        return 1
    stat = sp.call(CMD)
    if stat != 0:
        sys.exit("{} call exit with non-zero status")

    return 0

def CoreAnalysis(args):
    import os
    import sys
    import multiprocessing as mp
    import subprocess as sp
    from shutil import copyfile

    path = os.path.dirname(os.path.abspath(__file__))

    if args.stemplate:
        copyfile("{}/PipeConfigs/sample_sheet_example.txt".format(path),
                 "{}/sample_sheet_example.template".format(os.getcwd()))
        if not args.wtemplate:
            return 0

    if args.wtemplate:
        copyfile("{}/PipeConfigs/default_workflow.txt".format(path),
                 "{}/default_workflow.template".format(os.getcwd()))
        return 0

    if args.ssheet == "no_ss":
        sys.exit('''usage: ThePipeline CoreAnalysis [-h] -s SSHEET [-w WORKFLOW] [--sstemplate]
                                [--wtemplate] [-p PROC]
ThePipeline CoreAnalysis: error: argument -s/--sample-sheet is required
''')

    if args.workflow == "no_workflow_provided":
        workflow = "{}/PipeConfigs/default_workflow.txt".format(path)
    else:
        workflow = args.workflow

    if args.ssheet == "no_ssheet_provided":
        ssheet = "{}/PipeConfigs/sample_sheet_example.txt".format(path)
    else:
        ssheet = args.ssheet

    workflow_CMD = parse_workflow(workflow)

    for step in workflow_CMD:
        command = step[0]
        params = step[1:]
        if command != "kraken": # In Kraken multiprocessing cannot be used
                                # And the first sample must use preload
            pool = mp.Pool(args.proc) # A pool with the multiple processes to
                                      # be spawn
            tasks = [ [command, params, prefix, R1, R2] for prefix, R1, R2 in parse_sample_sheet(ssheet) ]
            stat = pool.map(call_command, tasks)
            if 1 in stat:
                sys.stderr.write("{} is not a command in CoreAnalysis\n".format(command))
            pool.close()
            pool.join()
        else:
            ssheet_generator = parse_sample_sheet(ssheet)
            prefix, R1, R2 = ssheet_generator.next()
            if "--preload" not in params:
                params.append("--preload")
            command, params, prefix, R1, R2
            call_command( [command, params, prefix, R1, R2] )
            params.remove("--preload")
            for prefix, R1, R2 in ssheet_generator:
                call_command( [command, params, prefix, R1, R2] )

    return 0
