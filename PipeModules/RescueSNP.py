def ParseMergeSNP(snp_fh, target):
    '''GENERATOR: Yield parsed lines of mergesnp file
    target is the index of the sample we are looking at
    each iteration'''
    for line in snp_fh:
        pos, ref, VarAllele, freqs = line.rstrip().split("\t", 3)
        freqs = freqs.split("\t")
        target_freq = freqs[target]
        # del freqs[target]

        yield (pos, VarAllele, target_freq, freqs)

def ParseSNP(snp_file):
    '''GENERATOR: Parse original .snp file from the file which SNPs
    are being rescued and yield parsed lines'''
    with open(snp_file) as infile:
        header = infile.readline()
        # I have to modify it so it does nor return VarAllele but the majority variant
        # therefore instead of a, I should return line[18]
        for line in infile:
            line = line.rstrip().split("\t")
            c, pos, r, a, r1, r2, f, s1, s2 = line[0:9]
            majority_variant = line[18]
            # At this point I substitute the variant for the majority variant
            # so in case is rescued it is already substituted
            line[3] = majority_variant
            line = "\t".join(line)
            line += "\n"
            yield ((pos, majority_variant, f, r1, r2, s2), line)

def RescueCandidate(nstrains, freqs, min_n_str, min_p_str):
    '''For a given row of freqs from a mergesnp table, decide
    if that position has to be rescued according to the minimum
    number/percentage of strains holding that variant'''
    NA_count = freqs.count("NA")
    DEL_count = freqs.count("DEL")
    calls = nstrains - (NA_count + DEL_count)
    if calls < min_n_str:
        return False

    calls_p = calls / float(nstrains)
    if calls_p < min_p_str:
        return False

    return True

def MergeSNPWalk(args):
    '''Walk through mergesnp table, checking NA positions for
    a given sample. If NA is found, apply rescue filters trying
    to recover the SNP'''
    candidate = []
    with open(args.mergesnp) as infile:
        header = infile.readline().rstrip().split("\t")
        samples = header[3:]
        nstrains = len(samples)
        # Get the target sample index in the parsed freqs list
        # I mean the column of the freqs for a given sample
        target = samples.index(args.prefix)
        for pos, VarAllele, target_freq, freqs in ParseMergeSNP(infile, target):
            if target_freq == "NA":
                if RescueCandidate(nstrains, freqs, args.min_num, args.min_perc):
                    candidate.append("{}@{}".format(pos, VarAllele))

    return candidate

def RescueSNP(args):
    '''For each SNP that is candidate for rescue, check if it
    pass the filters given by user, in that case, return the
    line of that variant from the original snp file
    '''

    candidate = MergeSNPWalk(args)
    rescued = []
    for snp_features, line in ParseSNP(args.rescue_from):
        pos, majority_variant, f, r1, r2, s2 = snp_features
        f = float(f.rstrip("%").replace(",", ".")) / 100.0
        key = "{}@{}".format(pos, majority_variant)
        if key in candidate:
            r1 = int(r1)
            r2 = int(r2)
            cov = r1 + r2
            if args.both_strands and s2 != "2":
                continue
            if r2 < args.min_reads2:
                continue
            if cov < args.min_cov:
                continue
            if f < args.min_freq:
                continue
            # We append also the position as an integer so wen we have
            # to add the line of the rescued snp, to de rescued snp file
            # we can sort by position and write the new file with ascending
            # variant positions
            # Add a final TRUE so we can know this is a rescued SNP
            line = line.rstrip()
            line += "\tTRUE\n"
            rescued.append((int(pos), line))

    return rescued

def Rescue_SNP_File(args):
    '''Open a given snp_file to rescue its snps'''
    import sys

    rescued = RescueSNP(args)
    with open(args.rescue_to) as infile:
        header = infile.readline().rstrip()
        if not header.startswith("Chrom"):
            sys.exit("Error in the snp file. Seems that it does not have a HEADER.")
        header += "\tRescued\n"
        for line in infile:
            c, pos, foo = line.split("\t", 2)
            line = line.rstrip()
            line += "\tFALSE\n"
            rescued.append((int(pos), line))

        # Sort by ascending positions
        rescued = sorted(rescued, key=lambda x:x[0])
        rescued.append(header)

    with open("{}.rescued".format(args.rescue_to), "w") as outfh:
        # Write header
        outfh.write(rescued.pop())
        # Write snp file
        for pos, line in rescued:
            outfh.write(line)

    return 0
