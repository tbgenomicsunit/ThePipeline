def Mdsam(prefix, reference, samtools, extension):
    '''Generate the mapped mdsam from $PREFIX.sort.bam files, with the command
    samtools fillmd and samtools view'''
    import subprocess as sp
    import sys

    sortbam = "{}.sort.{}".format(prefix, extension)
    mdsam_tmp = "{}.mdsam.tmp".format(prefix)
    mdsam = "{}.mdsam".format(prefix)
    with open(mdsam_tmp, "w") as outfh:
        stat = sp.call([samtools, "fillmd", "-e", sortbam, reference],
               stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped samtools fillmd\n")

    with open(mdsam, "w") as outfh:
        stat = sp.call([samtools, "view", "-hS", "-F4", mdsam_tmp],
               stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped samtools view -S -F4 mdsam\n")

    return 0

def ParseSam(samfile):
    '''GENERATOR: This function opens a sam file and yields line by line in a
    tuple with seq_name, alignment_start of the aligment CIGAR string and de
    sequence aligned'''

    with open(samfile) as infile:
        headers = []
        for line in infile:
            # Skip headers
            if line.startswith("@"):
                # First yield headers with parameters that will always
                # pass the filters
                yield(10000, "10000M", 10000*"=", line)
                continue
            tokens = line.split("\t")
            seqid = tokens[0]
            MAPQ = tokens[4]
            CIGAR = tokens[5]
            seq = tokens[9]
            yield (int(MAPQ), CIGAR, seq, line)

def ParseCIGAR(CIGAR):
    '''This function takes a CIGAR string as argument and returns a dictionary
    with counts for each cigar char'''
    import re

    cigar = {}
    matches = re.findall("\d+[A-Z]", CIGAR)
    for m in matches:
        char = m[-1]
        num = int(m[:-1])
        if char in cigar:
            cigar[char] += num
        else:
            cigar[char] = num

    return cigar

def AlignStats(cigar, seq):
    ''' Given a cigar string and the aligned sequences where = denotes
    a match and any other char (GACT) a mismatch, return a dictionary
    with number of matches, mismatches, indels and clipping
    '''
    matchs = seq.count("=")
    if "M" in cigar:
        mismatchs = cigar["M"] - matchs # Because in a perfect alignment this equals to 0
    else:
        mismatchs = 0
    if "I" in cigar:
        insertions = cigar["I"]
    else:
        insertions = 0

    ali_length = matchs + mismatchs + insertions
    matchs_p = float(matchs) / ali_length

    return (ali_length, matchs_p)

def FilterMdsam(prefix, minlen, minpident, minMAPQ):
    '''This function filters an mdsam according to provided parameters'''

    mdsam = "{}.mdsam".format(prefix)
    filtmdsam = "{}.filter.mdsam".format(prefix)
    with open(filtmdsam, "w") as outfile:
        for MAPQ, CIGAR, seq, line in ParseSam(mdsam):
            cigar = ParseCIGAR(CIGAR)
            ali_len, pident = AlignStats(cigar, seq)
            if ali_len >= minlen:
                if pident >= minpident:
                    if MAPQ >= minMAPQ:
                        outfile.write(line)
    return 0

def FilterMapping(args):
    '''This function filters a sort.bam file according to mapping stats. The
    unfiltered sort.bam is moved to nonfilter.sort.bam and the filtered sort.bam
    is written as PREFIX.sort.bam so it can be used in following Pipeline steps
    '''

    from Repository import Programs, Data
    import sys
    import shutil
    import subprocess as sp
    import os

    programs = Programs()
    data = Data()

    if args.reference == False:
    	reference = data["reference"]
    else:
  		reference = args.reference
    samtools = programs["samtools"]

    prefix = args.prefix
    minlen = args.minlen
    minpident = args.minpident
    minMAPQ = args.minMAPQ
    extension = args.extension

    Mdsam(prefix, reference, samtools, extension) # Produce an mdsam file
    FilterMdsam(prefix, minlen, minpident, minMAPQ) # Filter the mdsam

    # Move the old sort.bam
    shutil.move("{}.sort.{}".format(prefix, extension), "{}.sort.{}.NoMapFilter".format(prefix, extension))

    with open("{}.sort.cram".format(prefix, extension), "w") as outfh:
        stat = sp.call([samtools, "view", "-CS", "{}.filter.mdsam".format(prefix)],
               stdout=outfh)
        if stat != 0:
            sys.exit("Pipeline stopped at samtools view mdsam\n")
    # Generate a sort.bam file from filtered mdsam
    # view = sp.Popen([samtools, "view", "-bS", "{}.filter.mdsam".format(prefix)],
    #        stdout=sp.PIPE)
    # sort = sp.Popen([samtools, "sort", "-o", "{}.sort.bam".format(prefix), "-"],
    #        stdin=view.stdout)
    # view.stdout.close()
    # out, err = sort.communicate()

    # Remove files that we are not gonna use anymore
    os.remove("{}.mdsam.tmp".format(prefix))
    os.remove("{}.mdsam".format(prefix))
    # shutil.remove("{}.mdsam".format(prefix))
    os.remove("{}.filter.mdsam".format(prefix))

    return 0
