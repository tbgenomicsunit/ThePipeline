def Load_known_DR_mutations():
    '''Parse known resistance positions from phyresse'''
    import os

    path = os.path.dirname(os.path.abspath(__file__))
    phyresse = "{}/../tools/res/resistance_snps_list.csv".format(path)
    known_mutations = {}
    known_positions = []
    with open(phyresse) as infile:
        # Read HEADER
        infile.readline()
        infile.readline()
        for line in infile:
            line = line.rstrip().split("\t")
            pos = line[0]
            ref = line[3]
            var = line[4]
            gene = line[7]
            aa = line[11]
            codon = line[12]
            drug = line[14]
            conf_phyresse = line[15]
            conf_reseqtb = line[16]
            comments = "NA"
            if len(line) == 16:
                comments = line[17]
            key = "{}@{}@{}".format(pos, ref, var)
            known_mutations[key] = [gene, aa, codon, drug,
                                    conf_phyresse, conf_reseqtb, comments]
            known_positions.append(pos)

    return (known_mutations, known_positions)

def Load_DR_genes_positions():
    '''Get positions from resistance assoctiated genes parsing from
    ResistantGenes.bed'''
    import os
    path = os.path.dirname(os.path.abspath(__file__))

    rfile = "{}/../tools/res/ResistanceGenes.rpoCA.bed".format(path)
    DRgenPos = {}
    with open(rfile) as infile:
        for line in infile:
            chrom, start, end, gene, strand, drug = line.rstrip().split("\t")
            start = int(start)
            end = int(end)
            for i in xrange(start, end+1):
                DRgenPos[str(i)] = [gene, drug]

    return DRgenPos

def Parse_SNP(snp_file, indel=False):
    '''Given a snp_file / indel_file in VarScan format, parse the file and return
    pos, ref, var, major_var, freq, cov'''
    snps = {}
    snp_pos = []
    with open(snp_file) as infile:
        infile.readline()
        for line in infile:
            line = line.rstrip().split("\t")
            pos = line[1]
            ref = line[2]
            var = line[3]
            cov1 = int(line[4])
            cov2 = int(line[5])
            freq = line[6]
            if indel:
                type = "INDEL"
                major_var = line[16]
            else:
                type = "SNP"
                major_var = line[18]
            cov = cov1 + cov2
            key = "{}@{}@{}".format(pos, ref, major_var)
            snps[key] = type, var, freq, cov
            snp_pos.append(pos)

    return snps, snp_pos

def Parse_DEL(del_file):
    '''Given a .del file, parse the file and return all positions'''
    delpos = []
    with open(del_file) as infile:
        infile.readline()
        for line in infile:
            pos = line.split()[1]
            delpos.append(pos)

    return delpos

def Resistance_Predictor(args):

    prediction = {}

    known_mutations, known_positions = Load_known_DR_mutations()
    DRgenPos = Load_DR_genes_positions()
    snps, snp_pos = Parse_SNP(args.snp_file)
    indels, indel_pos = Parse_SNP(args.indel_file, indel=True)
    delpos = Parse_DEL(args.del_file)

    # make a single dict with snps and indels
    snps.update(indels)
    # For each of the known mutations in the resistance_snps_list check
    # if that variant has been called either in the provided snp or indel file
    for DR_var in known_mutations:
        pos, ref, var = DR_var.split("@")
        gene, aa, codon, drug, conf_phyresse, \
        conf_reseqtb, comments = known_mutations[DR_var]
        # If so, predict resistance and append info
        if DR_var in snps:
            type, major_var, freq, cov = snps[DR_var]
            prediction[DR_var] = ["Resistance", pos, type, ref, var, freq, cov,
                                   gene, drug, aa, codon, conf_phyresse,
                                   conf_reseqtb, comments]
        # Otherwise, if that position is in the .del file and not in the .indel
        # file. Then it should be because there was not enough coverage at
        # that position and then we say that position has not been assessed
        elif pos in delpos and pos not in indel_pos:
            type = "NA"
            alt_var = "NA"
            freq = "NA"
            cov = "NA"
            prediction[DR_var] = ["Position missed", pos, type, ref, var, freq, cov,
                                   gene, drug, aa, codon, conf_phyresse,
                                   conf_reseqtb, comments]

    # For each snp and indel that has been called
    for snp in snps:
        pos, ref, var = snp.split("@")
        # if its within a gene associated to DR
        if pos in DRgenPos:
            gene, drug = DRgenPos[pos]
            # and  is not directly associated to a known DR variant
            if snp not in known_mutations:
                type, alt_var, freq, cov = snps[snp]
                # but is within a position that DO has a known resistance variant
                if pos in known_positions:
                    # Append that info
                    prediction[snp] = ["Change in known DR position", pos, type, ref, \
                                           var, freq, cov, gene, drug, "NA", "NA", \
                                           "NA", "NA", "NA"]
                else:
                    prediction[snp] = ["Change in DR gene (Unknown position)", pos, type, ref, \
                                           var, freq, cov, gene, drug, "NA", "NA", \
                                           "NA", "NA", "NA"]
    return prediction

def Write_Prediction(prediction, prefix="no_prefix", output_unknown=False):
    '''From a prediction dict with following structure:

    prediction[pos@ref@var] = [gene, aa, codon, drug,
                               conf_phyresse, conf_reseqtb, comments]

    Write a DR prediction file with sample name if prefix if provided or to
    stdout otherwise
    '''
    import sys
    if prefix != "no_prefix":
        ofh = open("{}.DRprediction".format(prefix), "w")
    else:
        ofh = sys.stdout


    HEADER = "Prediction\tDrug\tGene\tPosition\tChange\tType\tFreq\tCov\tCodon\tAA\tConf_PhyResSE\tConf_ReSeqTB\tComments\n"
    ofh.write(HEADER)
    for snp in prediction:
        pred, pos, type, ref, var, freq, cov, \
        gene, drug, aa, codon, conf_phyresse, \
        conf_reseqtb, comments = prediction[snp]
        if pred == "Change in DR gene (Unknown position)" and not output_unknown:
            continue

        change = "{}>{}".format(ref, var)
        line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(pred,
                                                                 drug,
                                                                 gene,
                                                                 pos,
                                                                 change,
                                                                 type,
                                                                 freq,
                                                                 cov,
                                                                 codon,
                                                                 aa,
                                                                 conf_phyresse,
                                                                 conf_reseqtb,
                                                                 comments
                                                                )
        ofh.write(line)
    ofh.close()

def Resistance(args):
    prediction = Resistance_Predictor(args)
    Write_Prediction(prediction, args.prefix, args.output_unknown)
